/*********************************************************************************
 * This is an allocation algorithm that follows the First Fit allocation
 * policy, which always chooses the available slots with the lowest posible
 * index to serve the connection request. If the required slot or group of slots
 * is available, taking into account the spectrum contiguity and continuity
 * constraints, it creates the connections and returns 'ALLOCATED' to indicate
 * success; otherwise, it returns 'NOT_ALLOCATED' to indicate that the process
 * failed
 **********************************************************************************/

#include <fnsim/simulator.hpp>

BEGIN_ALLOC_FUNCTION(FirstFit)
{
  // Define auxiliary variables
  int currentNumberSlots;
  int currentSlotIndex;
  int requiredSlots = REQ_SLOTS_FIXED;
  std::vector<bool> totalSlots;

  // Iterate through all available routes
  for (int r = 0; r < NUMBER_OF_ROUTES; r++) {
    // Reset slot availability for the current route
    totalSlots = std::vector<bool>(LINK_IN_ROUTE(r, 0)->getSlots(), false);

    // Iterate through each link in the route
    for (int l = 0; l < NUMBER_OF_LINKS(r); l++) {
      // Update totalSlots vector with slot status from each link
      for (int s = 0; s < LINK_IN_ROUTE(r, l)->getSlots(); s++) {
        totalSlots[s] = totalSlots[s] | LINK_IN_ROUTE(r, l)->getSlot(s);
      }
    }

    // Search for a contiguous block of available slots
    currentNumberSlots = 0;
    currentSlotIndex = 0;

    for (int s = 0; s < totalSlots.size(); s++) {
      if (totalSlots[s] == false) { // Slot is available on all links
        currentNumberSlots++;
      }
      else { // Reset counter if a slot is occupied
        currentNumberSlots = 0;
        currentSlotIndex = s + 1;
      }

      // If a contiguous block of requiredSlots is found, allocate them
      if (currentNumberSlots == requiredSlots) {
        for (int l = 0; l < NUMBER_OF_LINKS(r); l++) {
          ALLOC_SLOTS(LINK_IN_ROUTE_ID(r, l), currentSlotIndex, requiredSlots);
        }
        return ALLOCATED;
      }
    }
  }
  // No suitable slot found in any route
  return NOT_ALLOCATED;
}
END_ALLOC_FUNCTION

int main(int argc, char *argv[])
{
  Simulator sim =
      Simulator(std::string("NSFNet.json"),
                std::string("routes.json")); // this creates the simulator
                                             // object using the JSON files
                                             // that contain the network
                                             // information
  USE_ALLOC_FUNCTION(FirstFit, sim);
  sim.setGoalConnections(1e6);
  sim.init();
  sim.run();

  return 0;
}