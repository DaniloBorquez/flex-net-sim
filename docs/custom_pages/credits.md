@page credits Credits

### **Third-Party Libraries Used**
Thanks to these libraries:
- [JSON for modern C++](https://github.com/nlohmann/json)
- [Catch2](https://github.com/catchorg/Catch2)

### Thanks

I really appreciate the help of the following people:
- [Álvaro Robert](https://gitlab.com/robstrings97)
- [Felipe Falcón](https://gitlab.com/ffalcon)
- [Gonzalo España](https://gitlab.com/GonzaloEspana)
- [Diana Mariño](https://gitlab.com/DianaMarino)
- [Mirko Zitkovich](https://gitlab.com/mirkozeta)