@page customization Customization

@tableofcontents

@section metrics Custom Metrics

@subsection bbp Bandwidth Blocking Probability
@todo Implement and explain BBP

@subsection throughput Throughput
@todo Implement and explain throughput

@section traffic Traffic Model

@subsection incremental Incremental Traffic
@todo Implement and explain Incremental Traffic

@subsection differentiated-bitrate Differentiated Traffic
@todo Implement and explain Differentiated Traffic

<div class="section_buttons">
|                        Read previous |
|:---------------------------------|
| [Usage Guide](@ref importing) |
</div>