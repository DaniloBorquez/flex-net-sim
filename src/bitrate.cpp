#include "bitrate.hpp"

#include <fstream>
#include <iostream>

BitRate::BitRate(double bitRate) {
  this->bitRate = bitRate;
  this->bitRateStr = std::to_string(bitRate);
}

BitRate::BitRate(const BitRate &bitRate){
  this->bitRate = bitRate.bitRate;
  this->bitRateStr = bitRate.bitRateStr;
  this->modulation = bitRate.modulation;
  this->reach = bitRate.reach;
  this->slots = bitRate.slots;
  this->bands = bitRate.bands;
  this->slotsPerBand = bitRate.slotsPerBand;
  this->reachPerBand = bitRate.reachPerBand;
}

std::vector<BitRate> BitRate::selectBitrateMethod(std::string fileName, int networkType) {
  switch (networkType) {
    case MB:
      return readBitRateFileMB(fileName);
      break;
    default:
      return readBitRateFile(fileName);
  }
}

BitRate::~BitRate() {}

void BitRate::addModulation(std::string modulation, int slots, double reach) {
  this->modulation.push_back(modulation);
  this->slots.push_back(slots);
  this->reach.push_back(reach);
}

void BitRate::addModulation(std::string modulation, int slots, double reach, std::vector<char> band,std::vector<int> slotsPerBand, std::vector<double> reachPerBand) {
  this->modulation.push_back(modulation);
  this->slots.push_back(slots);
  this->reach.push_back(reach);
  this->bands.push_back(band);
  this->slotsPerBand.push_back(slotsPerBand);
  this->reachPerBand.push_back(reachPerBand);
}

std::string BitRate::getModulation(int pos) {
  if (pos >= this->modulation.size()) {
    throw std::runtime_error(
        "Bitrate " + this->bitRateStr + " does not have more than " +
        std::to_string(this->modulation.size()) + " modulations.");
  }
  return this->modulation[pos];
}

char BitRate::getBand(int modulation,int pos){
  if (modulation >= this->bands.size()) {
    throw std::runtime_error(
        "Bitrate " + this->bitRateStr + " does not have more than " +
        std::to_string(this->bands.size()) + " modulations.");
  }
  if (pos >= this->bands[modulation].size()) {
    throw std::runtime_error(
        "Bitrate " + this->bitRateStr + " does not have more than " +
        std::to_string(this->bands[modulation].size()) + " bands.");
  }
  return this->bands[modulation][pos];

}
double BitRate::getReachPerBand(int modulation,int pos){
  if (modulation >= this->reachPerBand.size()) {
    throw std::runtime_error(
        "Bitrate " + this->bitRateStr + " does not have more than " +
        std::to_string(this->reachPerBand.size()) + " modulations.");
  }
  if (pos >= this->reachPerBand[modulation].size()) {
    throw std::runtime_error(
        "Bitrate " + this->bitRateStr + " does not have more than " +
        std::to_string(this->reachPerBand[modulation].size()) + " bands.");
  }
  return this->reachPerBand[modulation][pos];
}

int BitRate::getNumberOfSlotsPerBand(int modulation,int pos){
  if (modulation >= this->slotsPerBand.size()) {
    throw std::runtime_error(
        "Bitrate " + this->bitRateStr + " does not have more than " +
        std::to_string(this->slotsPerBand.size()) + " modulations.");
  }
  if (pos >= this->slotsPerBand[modulation].size()) {
    throw std::runtime_error(
        "Bitrate " + this->bitRateStr + " does not have more than " +
        std::to_string(this->slotsPerBand[modulation].size()) + " bands.");
  }
  return this->slotsPerBand[modulation][pos];
}

int BitRate::getNumberOfSlots(int pos) {
  if (pos >= this->slots.size()) {
    throw std::runtime_error(
        "Bitrate " + this->bitRateStr + " does not have more than " +
        std::to_string(this->slots.size()) + " modulations.");
  }
  return this->slots[pos];
}

double BitRate::getReach(int pos) {
  if (pos >= this->reach.size()) {
    throw std::runtime_error(
        "Bitrate " + this->bitRateStr + " does not have more than " +
        std::to_string(this->reach.size()) + " modulations.");
  }
  return this->reach[pos];
}

std::vector<BitRate> BitRate::readBitRateFile(std::string fileName) {
  std::ifstream file(fileName);
  nlohmann::ordered_json bitRate;
  std::vector<BitRate> vect = std::vector<BitRate>();

  file >> bitRate;

  for (auto& x : bitRate.items()) {
    int bitrate = stoi(x.key());  // BITRATE

    int numberOfModulations = x.value().size();

    BitRate aux = BitRate(bitrate);
    for (int i = 0; i < numberOfModulations; i++) {
      for (auto& w : x.value()[i].items()) {
        std::string modulation = w.key();
        int reach = w.value()["reach"];
        int slots = w.value()["slots"];

        // exceptions
        if (reach && slots < 0) {
          throw std::runtime_error(
              "value entered for slots and reach is less than zero");
        }

        if (reach < 0) {
          throw std::runtime_error("value entered for reach is less than zero");
        }

        if (slots < 0) {
          throw std::runtime_error("value entered for slots is less than zero");
        }
        aux.addModulation(modulation, slots, reach);
      }
    }
    vect.push_back(aux);
  }

  return vect;
}

std::vector<BitRate> BitRate::readBitRateFileMB(std::string fileName) {
  std::ifstream file(fileName);
  nlohmann::ordered_json bitRate;
  std::vector<BitRate> vect = std::vector<BitRate>();

  file >> bitRate;

  for (auto& x : bitRate.items()) {
    int bitrate = stoi(x.key());  // BITRATE

    int numberOfModulations = x.value().size();

    BitRate aux = BitRate(bitrate);
    for (int i = 0; i < numberOfModulations; i++) {
      for (auto& w : x.value()[i].items()) {
        std::string modulation = w.key();
        int numberOfBands = w.value().size();
        int slots=0;
        int reach=0;
        std::vector<char> band;
        std::vector<int> slotsPB;
        std::vector<double> reachPB;
        for (int j = 0; j < numberOfBands; j++) {
          for (auto& k : w.value()[j].items()){
              std::string charValue = k.key();
              band.push_back(charValue[0]);
              slotsPB.push_back(k.value()["slots"]);
              reachPB.push_back(k.value()["reach"]);
              reach+= k.value()["reach"].get<int>();
              slots+= k.value()["slots"].get<int>();
                        // exceptions
              if (reach && slots < 0) {
                throw std::runtime_error(
                    "value entered for slots and reach is less than zero");
              }

              if (reach < 0) {
                throw std::runtime_error("value entered for reach is less than zero");
              }

              if (slots < 0) {
                throw std::runtime_error("value entered for slots is less than zero");
              }
              

              }
              
            }
        aux.addModulation(modulation, slots, reach, band,slotsPB,reachPB);
      }

    }
    vect.push_back(aux);
  }

  return vect;
}

std::string BitRate::getBitRateStr() { return this->bitRateStr; }

double BitRate::getBitRate() { return this->bitRate; }

int BitRate::getNumberOfModulations() { return this->modulation.size(); }

int BitRate::getNumberOfBands() { return this->bands[0].size(); }
int BitRate::getNumberOfBands(int mod) { return this->bands[mod].size(); }

std::map<char, int> BitRate::getPosBands(int mod){
  if (mod >= this->bands.size()) {
    throw std::runtime_error(
        "Bitrate " + this->bitRateStr + " does not have more than " +
        std::to_string(this->bands.size()) + " modulations.");
  }
  std::map<char, int> posBands;
  for(int i=0;i<this->bands[mod].size();i++){
    posBands[this->bands[mod][i]]=i;
  }
  return posBands;
}