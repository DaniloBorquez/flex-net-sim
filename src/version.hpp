#ifndef __VERSION_H__
#define __VERSION_H__

static const unsigned VERSION_MAJOR = 0;
static const unsigned VERSION_MINOR = 8;
static const unsigned VERSION_REVISION = 2;

#endif