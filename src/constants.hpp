#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

// Network types
#define EON 1
#define SDM 2
#define MB 3

// Link default values
#define DEFAULT_SLOTS 320
#define DEFAULT_LENGTH 100.0
#define DEFAULT_CORES 1
#define DEFAULT_MODES 1
#define DEFAULT_BANDS 1

#endif